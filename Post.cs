﻿namespace MyWebApi
{
    public class Post
    {
        /// <summary>
        /// รหัสโพสต์
        /// </summary>
        /// <example>1</example>
        public int Id { get; set; }
        /// <summary>
        /// ชื่อโพสต์
        /// </summary>
        /// <example>หัวข้อ</example>
        public string Title { get; set; }
        /// <summary>
        /// เนื้อหาโพสต์
        /// </summary>
        /// <example>ตัวอย่างเนื้อหา</example>
        public string Body { get; set; }
    }

    public class PostDto
    {

        /// <summary>
        /// ชื่อโพสต์
        /// </summary>
        /// <example>หัวข้อ</example>
        public string Title { get; set; }
        /// <summary>
        /// เนื้อหาโพสต์
        /// </summary>
        /// <example>ตัวอย่างเนื้อหา</example>
        public string Body { get; set; }

    }
}
